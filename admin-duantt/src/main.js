import Vue from 'vue'
import App from './App.vue'
import routers from './routers/index'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false
Vue.use(ElementUI);
new Vue({
  router: routers,
  render: h => h(App),
}).$mount('#app')
