import Vue from "vue";
import Router from "vue-router";
import Dashboard from '../page/dashboard.vue';
import Table from "../page/table.vue";
import Form from "../page/form.vue";
Vue.use(Router);
const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "Dashboard",
            component: Dashboard,
        },
        {
            path: "/table-page",
            name: "Table",
            component: Table,
        },
        {
            path: "/form-page",
            name: "Form",
            component: Form,
        }
    ],
});
export default router;